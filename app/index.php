<?php
require_once __DIR__ . '/vendor/autoload.php';

$html = <<<'HTML'
<div>
	hogehoge
	<script>alert(1);</script>
	<textarea></textarea>
</div>
HTML;

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>

<body>
	hoge
	<hr>
	<?= (new DateTimeImmutable)->format('Y-m-d H:i:s') ?>
	<hr>
	<p>
		<textarea cols="30" rows="10"><?= h($html) ?></textarea>
	</p>
	<hr>
	<main></main>
	<script src="/dist/main.js"></script>
</body>

</html>
import { render } from 'preact'
import { useState } from 'preact/hooks'

const App = () => {
	const [count, setcount] = useState(0)
	return (
		<div>
			<p>
				<button onClick={() => setcount(c => 1 + c)}>up</button> {count}
			</p>
		</div>
	)
}

const main = () => {
	const el = document.querySelector('main')
	if (el) render(<App />, el)
}

main()

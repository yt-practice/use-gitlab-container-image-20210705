<?php

function h(string $in = null): string {
	if (null === $in) return '';
	return \htmlspecialchars($in, \ENT_QUOTES, 'UTF-8');
}
